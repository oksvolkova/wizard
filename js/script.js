$(document).ready(function () {
    $(".close").click(function () {
        $(this).closest('.item').hide();
    });

    $('.order .number').click(function () {
        $(this).parent().siblings('.οrder-content').slideToggle("fast");
    });

    $('.detaile a').click(function () {
        $('.οrder-content').slideToggle("fast");
    });

    $('.navigation').click(function () {
        $('.vertical-menu').slideToggle("fast");
    });

    $('.send-order a').click(function () {
        $('.adress-pop-up').show();
    });

    $(".close").click(function () {
        $('.adress-pop-up').hide();
    });

    $(".search").click(function () {
        $(this).css('width', '5%');
        $('input').css('left', '35px');
    })

    $(document).click(function handler(event) {
        var target = $(event.target);
        if (target.is($(".search input"))) {
            $(".search").css('width', '5%');
        } else {
            $(".search").css('width', '4%');
            $(".search input").css('left', '39%');
        }
    });
});
